package testdemo;

import static org.junit.Assert.assertEquals;

import org.junit.*;

public class Assignment8_3 {
	
	@Before
	public void abc2() {
		System.out.println("I am BeforeEach in Junit");
	}
	
	static OddEven obj;
	@BeforeClass
	public static void abc1() {
		obj = new OddEven();
		System.out.println("I am BeforeAll in Junit");
	}
	
	@Test
	public void abc3() {
		assertEquals(obj.isPalindrome("madam"), true);
		System.out.println("I am Test case for obj.isPalindrome(\"madam\") in Junit");
	}
	
	@Test
	public void abc8() {
		assertEquals(obj.isPalindrome("abcd"), false);
		System.out.println("I am Test case for obj.isPalindrome(\"abcd\") in Junit");
	}
	
	@After	
	public void abc4() {
		System.out.println("I am AfterEach in Junit");
	}
	
	@AfterClass
	public static void abc5() {
		System.out.println("I am AfterAll in Junit");
	}
	
}