package testdemo;

public class OddEven {

	public boolean isPalindrome(String str) {
		int l=str.length();
		boolean flag=true;
		
		for(int i=0; i<(l/2); i++) {
			if(str.charAt(i) != str.charAt(l-i-1)) {
				flag=false;
				break;
			}
		}
		return flag;
	}
}
