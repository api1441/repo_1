package testdemo;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Assignment8_4 {
	@Before
	public void abc2() {
		System.out.println("I am BeforeEach");
	}
	
	static Armstrong obj;
	@BeforeClass
	public static void abc1() {
		obj = new Armstrong();
		System.out.println("I am BeforeAll");
	}
	
	@Test
	public void abc3() {
		assertEquals(obj.isArmstrong(121), false);
		System.out.println("I am TestCase for isArmstrong(121)");
	}
	
	@Test
	public void abc8() {
		assertEquals(obj.isArmstrong(153), true);
		System.out.println("I am TestCase for isArsmstrong(153)");
	}
	
	@After
	public void abc4() {
		System.out.println("I am AfterEach");
	}
	
	@AfterClass
	public static void abc5() {
		System.out.println("I am AfterAll");
	}
}
